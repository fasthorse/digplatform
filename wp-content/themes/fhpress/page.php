<?php
/**
 * The template for displaying all pages
 *
*/
get_header(); ?>
<main role="main" class="main page">
	<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section role="section">
		<div class="row fullwidth center">
			<div class="col span12 ">
	
		
				<?php the_title('<h1>', '</h1>'); ?>
			
				<?php the_content(); ?>
	
					
			</div>
		</div>
	</section>
    
	<?php endwhile; endif; wp_reset_query(); ?>
</main>
<?php get_footer(); ?>
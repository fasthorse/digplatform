
<?php
	

	
?>

<section role="section" class="mission row content" id="mission">
	<div class="col span6">
		<h3>Our Mission</h3>
		<h1><?php echo get_field('mission_tagline'); ?></h1>
		<?php echo apply_filters('the_content', get_field('mission_content')); ?>
		
		<?php if(!empty(get_field('mission_link'))): ?><p><a href="<?php echo get_field('mission_link'); ?>" class="btn btn-fill"><?php echo get_field('mission_link_text'); ?></a></p><?php endif; ?>
	</div>
	<div class="col span6">
		<img src="<?php	echo get_field('mission_image'); ?>" class="mosaic left" alt=""/>
		<img src="<?php	echo get_field('mission_image_overlay'); ?>" class="overlay" alt=""/>
	</div>
</section>

<?php
	

	
?>
<?php
  $animation_duration = 5;
  $circle_counter_offset = 1;
  $text_counter_offset_x = 0.815;
  $text_counter_offset_y = 0.635;
  $location_boundary_margin = 1;  // Units, in percentage of mapcontainer, to give the location boundary as a margin so no location ends up on an absolute edge of the screen

  $args = array (
    'post_type'              => array( 'location' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true
  );

  $services = new WP_Query( $args );
  $map_image = null;
  $location_data = array();

  $boundary_data = array(
    'min_x' => 100,
    'max_x' => 0,
    'min_y' => 100,
    'max_y' => 0
  );

  // Get tags
  $tag_categories = array();
  foreach (FILTER_CATEGORIES as $index => $value) {
    $terms = get_terms( array(
        'taxonomy' => $index . '_tags',
        'hide_empty' => false
    ) );
    $tag_categories[$value] = $terms;
  }
?>

<section role="section" class="row map-row" id="map-row">
  <div class="story-headline isVisible">
	<h3>Our Stories</h3>
	<h1>See How We're Innovating Around The World</h1>
  </div>
  <div class="col span12 map" id="map" unselectable="on">
    <div id="coords-debug">
      <div id="posX"></div>
      <div id="posY"></div>
    </div>
    <div class="map-container" id="map-container">

<?php

      if ( $services->have_posts() ) {
        $count = 0;
        while ( $services->have_posts() ) {
          $services->the_post();
          $fields = get_post_meta(get_the_ID());
          $title = get_the_title();
          $location_id = get_the_ID();
          $location = explode(',',str_replace('%','',$fields['location'][0]));
          $location_tags = array();

          // A quick way to get animation variation between different locations, but mostly
          // spread it between 3 "ranges", all one second apart.
          $delay = ($count % 3) + (rand(1, 100) / 100);
          $count++;

          $location_data[$location_id]['x'] = $location[0];
          $location_data[$location_id]['y'] = $location[1];
          $location_data[$location_id]['delay'] = $delay;

          if ($location[0] < $boundary_data['min_x']) {
            $boundary_data['min_x'] = $location[0];
          } else if ($location[0] > $boundary_data['max_x']) {
            $boundary_data['max_x'] = $location[0];
          }

          if ($location[1] < $boundary_data['min_y']) {
            $boundary_data['min_y'] = $location[1];
          } else if ($location[1] > $boundary_data['max_y']) {
            $boundary_data['max_y'] = $location[1];
          }

          // Get stories!
          $args = array (
            'post_type'              => array( 'location_story' ),
            'post_status'            => array( 'publish' ),
            'nopaging'               => true,
            'meta_key'               => 'location',
            'meta_value'             => "$location_id"
          );

          $story_services = new WP_Query( $args );
          $number_of_stories = 0;
?>
          <location
            id="location-<?php echo $location_id; ?>"
            data-id="<?php echo get_the_ID(); ?>"
            data-title="<?php echo get_the_title(); ?>"
            data-x="<?php echo $location[0]; ?>"
            data-y="<?php echo $location[1]; ?>">
<?php
            if ($story_services->have_posts() ) {
              while ( $story_services->have_posts() ) {
                $number_of_stories++;
                $story_services->the_post();
                $story_fields = get_post_meta(get_the_ID());
                $title = get_the_title();
                $images = get_field('image_gallery');

                // Compile Tags
                $story_tags = array();
                foreach(FILTER_CATEGORIES as $index => $value) {
                  $category_tags = get_field($index . '_tags');
                  if ($category_tags) {
                    $story_tags = array_merge($story_tags, $category_tags);
                  }
                }
                $location_tags = array_merge($location_tags, $story_tags);

                // Compile group links
                $group_links = array();
                while ( have_rows('group_links') ) : the_row();
                  $group_links[] = array(
                    'link_text' => get_sub_field('link_text'),
                    'group_link' => get_sub_field('group_link')
                  );
                endwhile;

                // Compile related stories
                $related_stories = array();
                while ( have_rows('related_stories') ) : the_row();
                  $related_stories[] = get_sub_field('related_story');
                endwhile;
?>
                <story
                  id="story-<?php echo get_the_ID(); ?>"
                  data-id="<?php echo get_the_ID(); ?>"
                  data-title="<?php echo $title; ?>"
                  data-story-video-src="<?php echo $story_fields['story_video'][0]; ?>"
                  data-story-video-poster="<?php echo wp_get_attachment_url($story_fields['story_video_poster'][0]) ?>"
                  data-accent-image-src="<?php echo wp_get_attachment_url($story_fields['accent_image'][0]); ?>"
                  data-header-text="<?php echo $story_fields['header_text'][0] ?>"
                  data-button-url="<?php echo $story_fields['button_url'][0] ?>"
                  data-tag-ids="<?php echo implode(',', $story_tags); ?>"
                  data-button-cta="<?php echo $story_fields['button_cta'][0] ?>"
                  data-related-story-ids="<?php echo implode(',',$related_stories) ?>">
                  <div class="story-body-text">
                    <?php echo $story_fields['body_text'][0]; ?>
                  </div>
                  <gallery>
                     <?php foreach( $images as $image ): ?>
                        <galleryimage
                          data-thumbnail="<?php echo $image['sizes']['story-thumbnail']; ?>"
                          data-fullsize="<?php echo $image['url']; ?>">
                        </galleryimage>
                    <?php endforeach; ?>
                  </gallery>
                  <excerpt>
                    <?php if(!empty($story_fields['excerpt_text'][0])) { echo $story_fields['excerpt_text'][0]; } else { echo $title; } ?>
                  </excerpt>
                  <grouplinks>
<?php
                    foreach ($grouplinks as $link) {
?>
                    <grouplink
                      data-text="<?php $link['link_text']; ?>"
                      data-url="<?php $link['group_link']; ?>"></grouplink>
<?php
                    }
?>
                </grouplinks>
              </story>
<?php
              }
            }
            $location_data[$location_id]['num_stories'] = $number_of_stories;
            $location_tags = array_unique($location_tags);
?>
            <tags data-tag-ids="<?php echo implode(',', $location_tags); ?>"></tags>
          </location>
<?php
          if (!$map_image) {
            $map_image = wp_get_attachment_url($fields['map_image'][0]);
          }
        }
      }
?>
      <locationboundaries
        data-max-x="<?php echo $boundary_data['max_x']; ?>"
        data-max-y="<?php echo $boundary_data['max_y']; ?>"
        data-min-x="<?php echo $boundary_data['min_x']; ?>"
        data-min-y="<?php echo $boundary_data['min_y']; ?>">
      </locationboundaries>
      <svg width="100%" height="100%">
        <?php
          foreach ($location_data as $key => $value) {
?>
          <svg class="location-svg" id="location-svg-<?php echo $key; ?>" data-location-id="<?php echo $key; ?>">
            <circle id="location-hotspot-<?php echo $key; ?>"
                class="map-circle"
                cx="<?php echo $value['x']; ?>%"
                cy="<?php echo $value['y']; ?>%"
                r=".33%">
              <animate attributeName="fill" 
                  begin="<?php echo $value['delay']; ?>s" 
                  dur="<?php echo $animation_duration; ?>s" 
                  repeatCount="indefinite" 
                  from="#FFFFFF" 
                  to="#DAAA26" />
            </circle>
            <?php
            if ($value['num_stories'] > 1) : ?>
            <circle id="aura-<?php echo $key ?>" 
                class="map-circle-aura" 
                cx="<?php echo $value['x']; ?>%" 
                cy="<?php echo $value['y']; ?>%" 
                r="1.5%">
                <animate attributeName="r" 
                    begin="<?php echo $value['delay']; ?>s"
                    dur="<?php echo ( $animation_duration * 0.75 ); ?>s" 
                    repeatCount="indefinite" 
                    from="1.5%" 
                    to="3%"/>
                <animate class="aura-opacity-animation" attributeName="stroke-opacity" begin="<?php echo $value['delay']; ?>s" dur="<?php echo ( $animation_duration * 0.75 ); ?>s" repeatCount="indefinite" from="1" to="0"/>
            </circle>
            <?php else :  ?>
            <circle id="aura-<?php echo $key ?>" 
                class="map-circle-aura" 
                cx="<?php echo $value['x']; ?>%" 
                cy="<?php echo $value['y']; ?>%" 
                r="0.33%">
                <animate attributeName="r" 
                    begin="<?php echo $value['delay']; ?>s"
                    dur="<?php echo $animation_duration; ?>s" 
                    repeatCount="indefinite" 
                    from="0.33%" 
                    to="3%"/>
                <animate class="aura-opacity-animation" attributeName="stroke-opacity" begin="<?php echo $value['delay']; ?>s" dur="<?php echo $animation_duration; ?>s" repeatCount="indefinite" from="1" to="0"/>
            </circle>
            <?php endif; ?>
        <?php
            if ($value['num_stories'] > 1) {
?>
          <svg class="counter-circle" id="counter-circle-<?php echo $key; ?>" data-count="<?php echo $value['num_stories']; ?>">
            <circle class="map-counter-aura" 
                cx="<?php echo $value['x']; ?>%" 
                cy="<?php echo $value['y']; ?>%" 
                r="1.5%">
            </circle>
            <circle class="map-counter-circle"
                cx="<?php echo $value['x'] + $circle_counter_offset; ?>%"
                cy="<?php echo $value['y'] - $circle_counter_offset; ?>%"
                r=".65%"/>
              
            <text class="map-circle-text"
                  x="<?php if($value['num_stories'] <= 10) { echo $value['x'] + $text_counter_offset_x; } else { echo $value['x'] + ($text_counter_offset_x * 0.8); } ?>%"
                  y="<?php echo $value['y'] - $text_counter_offset_y; ?>%"><?php echo $value['num_stories']; ?></text>
            
          </svg>
<?php
            }
?>
        </svg>
<?php
          }
?>
      </svg>
      <img id="map-image" class="map-image" src="<?php echo get_bloginfo("stylesheet_directory"); ?>/images/Map_opt.jpg" />

    </div>


    <div id="filter-button" class="filter-button">
      <span class="icon icon-filter"></span>
    </div>

    <?php
      include( locate_template( 'partials/_story_info.php', false, false ) ); 
      include( locate_template( 'partials/_location_info.php', false, false ) ); 
      include( locate_template( 'partials/_filters.php', false, false ) ); 
    ?>

  </div>
</section>

<?php wp_reset_postdata(); ?>
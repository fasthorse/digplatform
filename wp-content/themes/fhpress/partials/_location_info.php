<div id="location-info" class="col span4 location-info">
  <div id="location-close-button" class="close-button">
	  <span class="icon icon-close"></span>
  </div>
  <div id="location-header" class="location__hd">
  </div>
  <div id="location-info-body" class="location__bd">
    <ul id="story-summaries" class="story-summaries">
    </ul>
  </div>
</div>
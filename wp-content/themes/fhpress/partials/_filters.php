<div id="filter-drawer" class="col span4 filter-drawer">
  <div id="filter-close-button" class="close-button">
    <span class="icon icon-close"></span>
  </div>
  <div class="filter-head">
	<h5><span class="icon icon-filter"></span>&nbsp;Filter</h5> 
	<div class="radio-container"> 
	    <input type="radio" name="select-all" class="filter-radio" id="filters-select-all" checked>
	    <label for="filters-select-all"><span>Select All</span></label>
	    <div class="check"></div>
	</div>
  </div>
<?php
  foreach ($tag_categories as $index => $value) {
?>
  <div class="filter-category">
	    <h6><?php echo $index; ?></h6>
<?php foreach ($value as $tag) { 
        $tag_id = $index . '-' . $tag->term_id;
?>

      <div class="category-tag">
	    <div class="radio-container">
	        <input type="radio" name="<?php echo $index; ?>" class="filter-radio filter-tag-radio" id="<?php echo $tag_id ?>" value="<?php echo $tag->term_id; ?>">
	        <label for="<?php echo $tag_id; ?>"><span><?php echo $tag->name; ?></span></label>
	        <div class="check"></div>
	    </div>
      </div>

    <?php } ?>
  </div>
  <div style="clear:both"></div>
<?php
  }
?>
</div>
<div id="story-info" class="col span4 story-info">
  <div id="story-close-button" class="close-button">
	  <span class="icon icon-close"></span>
  </div>
  <div class="story__hd" id="story__hd">
    <div id="story-info-video-container">
      <a href="#" class="play btn btn-fill round" id="story-info-video-src" data-video-src=""><span class="icon icon-play"></span> Play Video</a>
      <img id="story-info-video-poster" src=""/>
      
    </div>
    <img id="story-info-accent-img" class="story-info-accent-img" src="">
    <div id="story-info-gallery" class="story-info-gallery" data-featherlight-gallery
      data-featherlight-filter="a">
    </div>
  </div>
  <div class="story__bd">
	<div id="story-info-tags" class="story-info-tags">
	</div>
    <h2 id="story-info-header" class="story-info-header">
    </h2>
    <p id="story-info-body" class="story-info-body">
    </p>
    <div id="story-info-button-div" class="story-info-button-div">
    </div>
    <div id="story-info-group-links">
    </div>
    <div id="related-stories" class="story-summaries">
    </div>
    <div class="story-footer">
	    <a href="#" class="back-to-map"><span class="icon icon-arrow-right"></span> Back</a>
	   
    </div>
    
  </div>
  <div class="read-more">
	  <p> <span class="icon icon-arrow-right"></span> scroll for more</p>
  </div>
</div>

<?php
	

	
?>

<section role="section" class="news row content" id="news">
	<div class="col span12">
		<h3>Cargill Innovation</h3>
		<h1>News & Updates</h1>
		
		<div class="news">
			
			<?php if(have_rows("posts")) : while(have_rows("posts")) : the_row(); ?>
			<div class="post main">
				<img src="<?php echo get_sub_field('image'); ?>" class="mosaic right" alt="" />
				<p class="source"><?php echo get_sub_field('publication'); ?></p>
				<h6><?php echo get_sub_field("title"); ?></h6>
				
				<p><a href="<?php echo get_sub_field("url"); ?>" class="btn btn-fill"><span class="icon icon-arrow-right"></span></a></p>
				
			</div>
			<?php endwhile; endif; wp_reset_query(); ?>
			
			
			
		</div>
		
	</div>
	
</section>

<?php
	

	
?>
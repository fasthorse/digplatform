  $( document ).ready(function() {
    const zoomSpeed = 750;
    const selectedZoomEffect = "200%";
    const selectedZoomModifier = 2;
    const fontRatio = 0.65;
    const northPoleMargin = .15; // The percentage of the top of the map to initially have panned out
    const boundaryMargin = .05; // The percentage of the boundary margin to give locations near the edge of the default zoom
	//console.log(defaultFontSize);
    var selectedStoryId = 0;
    var selectedLocationId = 0;
    var defaultFontSize = ((( $(window).innerWidth() * fontRatio ) / 100 ) + 'px'); // Needs to be copied from css. jQuery can't get a non pixel version of font size
    var defaultZoomEffect = "100%";
    var defaultZoomModifier = 1;
    var defaultMapHeight = 0;
    var defaultMapWidth = 0;
    var defaultTop = 0;
    var defaultLeft = 0;
    var zoomDelay = 0;
    //var videoElement = $('#story-info-video');
    //videoElement.remove();
   

    // A boolean value as a lazy method of determining if the page is loading
    // right now just used in zoomToDefault so we don't animate going to 
    // default zoom on page load
    var loading = true;


    // Ensure map can't be dragged beyond boundaries
    $('#map-container').draggable(
      {
        drag: function(event, ui) {
        var position = $(this).position();
        var xPos = position.left;
        var yPos = position.top;
        var mapContainer = $('#map-container');
        var map = $('#map');
        var infoDrawer = $('#location-info');
        var drawerOffset = 0;

        if (mapContainer.width() > map.width()) {
          // user is zoomed in, so let the map scroll beyond the right boundary
          // so we can zoom to what's under the drawer
          drawerOffset = infoDrawer.width();
        }

        if (ui.position.left < map.width() - mapContainer.width() - drawerOffset) { 
          ui.position.left = map.width() - mapContainer.width() - drawerOffset;
        }
        if (ui.position.left > 0) { 
          ui.position.left=0;
        }
        if (ui.position.top < map.height() - mapContainer.height()) { 
          ui.position.top = map.height() - mapContainer.height();
        }
        if (ui.position.top > 0) { 
          ui.position.top = 0;
        }

        var mapRow = $('#map-row');
        mapRow.css('background-position', ui.position.left + 'px ' + ui.position.top + 'px');
      }
    });

    $('.close-button, .back-to-map').click(function(e) {
	    e.preventDefault();
	  if (selectedStoryId != 0) {
      		$('.filter-tag-radio').prop('checked', false);
	  		updateFilteredLocations();
	  		resetFilters();
      }
		hideDrawers();
    });
    
    function resetFilters() {
      $('.filter-tag-radio').prop('checked', false);
      $('#filters-select-all').prop('checked', true);
      updateFilteredLocations();
    }


    $('#filter-button').click(function() {
      showFilters();
    });

    $('.filter-tag-radio').click(function() {
      $('#filters-select-all').prop('checked', false);
      updateFilteredLocations();
    });

    $('#filters-select-all').click(function() {
           resetFilters();
    });

    function getSelectedFilters() {
      var selectedFilters = [];

      $('.filter-tag-radio:checked').each(function (index, tag) {
        selectedFilters.push(tag.value);
      });

      return selectedFilters;
    }

    function updateFilteredLocations() {
      // clear previous filters

      $('.location-svg').removeClass('isHidden');
      $('.counter-circle').removeClass('isHidden');
      $('.counter-circle').each(function (index, element) {
        element = $(element);
        element.children('.map-circle-text').html(element.data('count'));
      });

      $('.aura-opacity-animation').each(function (index, element) {
        element = $(element);
        element.attr('from', '1');
      });

      // apply currently selected filters
      var selectedFilters = getSelectedFilters();

      // for each location, see if it has any matching tags across all of its stories
      // if not, apply hidden to that location graphic
      if (selectedFilters.length > 0) {
        $('location').each(function (index, location) {
          location = $(location);
          locationId = location.data('id');
          locationTags = location.children('tags').first().data('tag-ids').toString().split(',');

          var matchedFilters = 0;
          selectedFilters.forEach(function(item, index) {
            if (locationTags.includes(item)) { matchedFilters++; }
          });
          if (matchedFilters < selectedFilters.length) {
            $('#location-svg-' + locationId).addClass('isHidden');
            // Because SVG 2 implementation across browsers is spotty at best,
            // and we can't animate circle radius in CSS without it,
            // we needed to use SMIL for animations. So we can't just apply a class to
            // hide the aura, we have to go in and alter the animate element...
            var auraAnimation = $('#aura-' + location.data('id')).children('.aura-opacity-animation').first();
            auraAnimation.attr('from', '0');
          } else {
            if (location.children('story').length == 1) {
              // no counter auras!
              $('#counter-circle-' + locationId).addClass('isHidden');
            } else {
              // Find how many child stories match the filters
              var matchedStories = 0;

              location.children('story').each(function (index, story) {
                story = $(story);
                storyTags = story.data('tag-ids').toString().split(',');

                var matchedFilters = 0;
                selectedFilters.forEach(function(item, index) {
                  if (storyTags.includes(item)) { matchedFilters++; }
                });

                if ( matchedFilters == selectedFilters.length ) {
                  matchedStories++;
                }
              });

              if (matchedStories == 1) {
                // no counter auras!
                $('#counter-circle-' + locationId).addClass('isHidden');
              } else {
                // update match count
                $('#counter-circle-' + locationId).children('.map-circle-text').html(matchedStories);
              }
            }
          }
        });
      }
    }

    $('.location-svg').click(function() {
      var locationId = $(this).data('location-id');
      selectLocation(locationId);
    });

    $('#story-info-video-container').hover(
      function() {
        $('#story-info-accent-img').addClass('isReceded');
      },
      function() {
        $('#story-info-accent-img').removeClass('isReceded');
      }
    );

    function drawerIsOpen() {
	    zoomDelay = 500;
      return ($('#location-info').hasClass('isVisible')
          || $('#story-info').hasClass('isVisible')
          || $('#filter-drawer').hasClass('isVisible'));
    }

    function isZoomedIn() {
      return (Math.floor($('#map-container').width()) == Math.floor($('#map-row').width()) * selectedZoomModifier);
    }

    function selectStory(storyId) {
      if (drawerIsOpen()) {
        hideDrawers(false);
        selectedStoryId = storyId;
        
      } else {
        showSelectedStory();
      }
    }

    function selectLocation(locationId) {
      if (locationId != selectedLocationId) {
  

        if (drawerIsOpen()) {
          // This hide will end up triggering a "show" function when the animation completes
          if (selectedLocationId || selectedStoryId) {
            // The map is already zoomed in, so don't zoom out and then back in again
            hideDrawers(false);
          } else {
            hideDrawers();
          }
        }

        selectedLocationId = locationId;
        zoomToSelectedLocation();

        $('#aura-' + locationId).addClass('selected-aura');

        var stories = $('#location-' + locationId).children('story');

        if (stories.length == 1) {
          // Single Story Location.
          selectedStoryId = stories.first().data('id');
          if (!drawerIsOpen()) {
            showSelectedStory();
          }
        } else {
          // Multi Story Location.
          if (!drawerIsOpen()) {
            showSelectedLocation();
          }
        }
      }
    }

    function showSelectedLocation() {
      var newHeader = $('<h2>').html($('#location-' + selectedLocationId).data('title'));
      $('#location-header').append(newHeader);

      var stories = $('#location-' + selectedLocationId).children('story');
      var selectedFilters = getSelectedFilters();
      var filteredStories = [];

      stories.each(function (index, story) {
        story = $(story);

        if (selectedFilters.length > 0) {
          // Check to see if this story matches current filters
          var storyTags = story.data('tag-ids').toString().split(',');

          var match = 0;
          selectedFilters.forEach(function(item, index) {
            if (storyTags.includes(item)) { match++; }
          });

          if (match == selectedFilters.length) {
            filteredStories.push(story);
          }
        } else {
          filteredStories.push(story);
        }
      });

      if (filteredStories.length == 1) {
        // All stories but one are filtered out, just go straight to showing it
        selectedStoryId = filteredStories[0].data('id');
        showSelectedStory();
      } else {
        filteredStories.forEach(function (story, index) {
          var newStoryItem = $('<li>', { id: 'story-summary-' + story.data('id'), 'data-story-id': story.data('id'), class: 'story-summary' });

          var imageDiv = $('<div>', { class: 'story-summary-image' });
          var newImage = $('<img>', { src: story.children('gallery').children('galleryimage').first().data('thumbnail') });
          imageDiv.append(newImage);

          var excerptDiv = $('<div>', { class: 'story-summary-excerpt' });
          var newSpan = $('<span>');
          newSpan.html(story.children('excerpt').first().html());
          excerptDiv.append(newSpan);

          newStoryItem.append(imageDiv);
          newStoryItem.append(excerptDiv);
          $('#story-summaries').append(newStoryItem);
        });

        $('.story-summary').click(function() {
          selectStory(this.getAttribute('data-story-id'));
        });

        $('#location-info').addClass('isVisible');
      }
    }

    function showSelectedStory() {
      var storyId = selectedStoryId;
      $('#story-info').addClass('isVisible');
      var story = $('#story-' + storyId);
	  var featuredImage = false;
      $('#story-info-header').html(story.data('header-text'));
      $('#story-info-accent-img').attr('src', story.data('accent-image-src'));
      
      $('#story-info-body').html(story.children('div').first().html());

      if (story.data('story-video-src') != '') {
	    $('#story-info-video-container').show(0);
        //$('#story-info-video-container').prepend(videoElement);
        $('#story-info-video-poster').attr('src', story.data('story-video-poster'));
        $('#story-info-video-src').attr('data-video-src', story.data('story-video-src'));
		$('#story-info-gallery').removeClass('has-featured-image');
      } else {
	      $('#story-info-video-container').hide(0);
	       var featuredImage = true;
	       $('#story-info-gallery').addClass('has-featured-image');
      }

      story.children('gallery').children('galleryimage').each(function (index, image) {
        image = $(image);
        if(index==0 && featuredImage) {
	         var newAnchor = $('<a>', { href: image.data('fullsize'), class: 'featured-image', target: "_blank", "data-featherlight" : "image" });
        } else {
			var newAnchor = $('<a>', { href: image.data('fullsize'), target: "_blank", "data-featherlight" : "image" });
		}
        var newImage = $('<img>', { src: image.data('thumbnail'), class: "story-info-gallery-image"});
        newAnchor.append(newImage);
        $('#story-info-gallery').append(newAnchor);
      });

      if (story.children('grouplinks').length > 0) {
        var newLinkList = $('<ul>', { class: "story-info-group-links" });

        story.children('grouplinks').children('grouplink').each(function (index, link) {
          link = $(link);
          var newListItem = $('<li>');
          var newLink = $('<a>', { href: link.data('url'), target: "_blank" });
          newLink.html(link.data('text'));
          newListItem.append(newLink);
          newLinkList.append(newListItem);
        });
        $('#story-info-group-links').append(newLinkList);
      }

      if (story.data('button-url')) {
        var newButton = $('<a>', { href: story.data('button-url'), target: "_blank", class:"btn btn-fill round", id: "story-info-button" });
        newButton.html(story.data('button-cta'));
        $('#story-info-button-div').append(newButton);
      }
      if (story.data('related-story-ids')) {
        var newLinkList = $('<ul>', { class: "related-stories" });
        newLinkList.append( $('<h2>').html('Related Stories') );

        story.data('related-story-ids').split(",").forEach(function(storyId, index) {
          var story = $("#story-" + storyId);
          var newStoryItem = $('<li>', { 'data-story-id': story.data('id'), class: 'story-summary' });

          var imageDiv = $('<div>', { class: 'story-summary-image' });
          var newImage = $('<img>', { src: story.children('gallery').children('galleryimage').first().data('thumbnail') });
          imageDiv.append(newImage);

          var excerptDiv = $('<div>', { class: 'story-summary-excerpt' });
          var newSpan = $('<span>');
          newSpan.html(story.children('excerpt').first().html());
          excerptDiv.append(newSpan);

          var clearDiv = $('<div>', { style: 'clear: both' });

          newStoryItem.append(imageDiv);
          newStoryItem.append(excerptDiv);
          newStoryItem.append(clearDiv);
          newLinkList.append(newStoryItem);
        });

        $('#related-stories').append(newLinkList);
        
        
      }
      readMore();
    }

    function hideDrawers( doZoomToDefault = true ) {
      // Don't just hide the drawers, but clear out any divs that could
      // cause jank when the sliders come back before new images/video is loaded

      // Hide Filters
      $('#filter-drawer').removeClass('isVisible');
      $('#location-header').html('');

      // Hide location
      $('#story-summaries').html('');
      $('#location-info').removeClass('isVisible');
      $('#aura-' + selectedLocationId).removeClass('selected-aura');
      selectedLocationId = 0;

      // Hide story
      $('#story-info').removeClass('isVisible');
      $('#story-info-accent-img').removeClass('isReceded');
      $('#story-info-gallery').empty();
      $('#story-info-button-div').empty();
      $('#story-info-video-src').attr('data-video-src', '');
      $('#story-info-accent-img').attr('src', '');
      $('#story-info-header').html('');
      $('#story-info-body').html('');
      $('#story-info-group-links').html('');
      $('#related-stories').html('');
      //videoElement.remove();
      selectedStoryId = 0;

	  
      if (doZoomToDefault) {
        zoomToDefault();
      }
    }

    function showFilters() {
      //hideDrawers();
      hideTitle();
      $('#filter-drawer').addClass('isVisible');
      var mapContainer = $('#map-container');
      var mapRow = $('#map-row');
      var filterDrawer = $('#filter-drawer');
      var newWidth = mapContainer.width();
      var newHeight = mapContainer.height();
      

      //if (mapContainer.width() == mapRow.width()) {
        // We're going to be zooming in, so calculate center based on new values
        //newWidth = newWidth * selectedZoomModifier;
        //newHeight = newHeight * selectedZoomModifier;
        //newFontSize = newFontSize * selectedZoomModifier;
      //}

      var calculatedTop = (newHeight - (mapRow.height()/1)) * -1;
      var calculatedLeft = (newWidth - (mapRow.width()/3)) * -1;

      if (calculatedLeft < mapRow.width() - newWidth - filterDrawer.width()) { 
        calculatedLeft = mapRow.width() - newWidth - filterDrawer.width();
      }
      if (calculatedLeft > 0) { 
        calculatedLeft = 0;
      }
      if (calculatedTop < mapRow.height() - newHeight) { 
        calculatedTop = mapRow.height() - newHeight;
      }
      if (calculatedTop > 0) { 
        calculatedTop = 0;
      }
	  
      mapContainer.animate( { top: calculatedTop, left: calculatedLeft  }, zoomSpeed );
      //mapRow.animate( { 'background-position-x': calculatedLeft, 'background-position-y': calculatedTop }, zoomSpeed);
    }

    function zoomToSelectedLocation() {
	 
	  //blur();
	  hideTitle();
      var mapContainer = $('#map-container');
      var mapRow = $('#map-row');
      var infoDrawer = $('#location-info');
      var location = $('#location-' + selectedLocationId);
      var newWidth = mapContainer.width();
      var newHeight = mapContainer.height();
      var newFontSize = ((( $(window).innerWidth() * 1.15 ) / 100 ) + 'px');

      if (!isZoomedIn()) {
        // we're at default zoom, so we'll be zooming into selected.
        // adjust width and height calculations accordingly
        newWidth = newWidth * (selectedZoomModifier/defaultZoomModifier);
        newHeight = newHeight * (selectedZoomModifier/defaultZoomModifier);
      }

      if (mapContainer.width() != mapRow.width() * selectedZoomModifier) {
        // We're going to be zooming in, so calculate center based on new values
//        newWidth = newWidth * selectedZoomModifier;
  //      newHeight = newHeight * selectedZoomModifier;
        //newFontSize = newFontSize * selectedZoomModifier;
      }

      var positionX = location.data('x');
      var positionY = location.data('y');

      var calculatedTop = (newHeight * (positionY / 100) - (mapRow.height()/2)) * -1;
      var calculatedLeft = (newWidth * (positionX / 100) - (mapRow.width()/3)) * -1;

      if (calculatedLeft < mapRow.width() - newWidth - infoDrawer.width()) { 
        calculatedLeft = mapRow.width() - newWidth - infoDrawer.width();
      }
      if (calculatedLeft > 0) { 
        calculatedLeft = 0;
      }
      if (calculatedTop < mapRow.height() - newHeight) { 
        calculatedTop = mapRow.height() - newHeight;
      }
      if (calculatedTop > 0) { 
        calculatedTop = 0;
      }
      setTimeout( function() {
	       $('.map-circle-text').css( 'font-size', newFontSize );
		   mapContainer.animate( { top: calculatedTop, left: calculatedLeft, width: selectedZoomEffect }, zoomSpeed, function() { $('.location-svg').addClass('isZoomed'); redrawMap(); /*unblur(); */} );
		  // mapRow.animate( { backgroundSize: selectedZoomEffect, 'background-position-x': calculatedLeft, 'background-position-y': calculatedTop }, zoomSpeed);
		  
      }, zoomDelay);
     
    }

    function calculateDefaultBoundaries() {
      var locationBoundariesElement = $('locationboundaries').first();
      var mapContainer = $('#map-container');
      var containerWidth = mapContainer.width();
      var containerHeight = mapContainer.height();
      var mapRow = $('#map-row');
      var rowWidth = mapRow.width();
      var rowHeight = mapRow.height();

      if (containerWidth != rowWidth) {
        // map is not at zoom 100%
        // adjust containerWidth and Height to calculate against zoom 100%
        let adjustment = containerWidth / rowWidth;
        containerWidth = containerWidth / adjustment;
        containerHeight = containerHeight / adjustment;
      }
      // Determine the boundariesin pixels based on the percentage of the locations closest to
      // any side of the map (adjusting for the location_boundary_margin defined in stories.php)
      var maxX = ((locationBoundariesElement.data('max-x') / 100) * containerWidth) + (containerWidth * boundaryMargin);
      var minX = ((locationBoundariesElement.data('min-x') / 100) * containerWidth) - (containerWidth * boundaryMargin);
      var maxY = ((locationBoundariesElement.data('max-y') / 100) * containerHeight) + (containerHeight * boundaryMargin);
      var minY = ((locationBoundariesElement.data('min-y') / 100) * containerHeight) - (containerHeight * boundaryMargin);

      // The boundary must be in proportion to the map row div, not the map image
      // determine if the boundary is wider or taller in relation to the container, 
      // and then expand the narrower dimension to the scale of the map row
      // while keeping it centered
      var boundaryWidth = maxX - minX;
      var boundaryHeight = maxY - minY;
      var rowRatio = rowWidth / rowHeight;
      var boundaryRatio = boundaryWidth / boundaryHeight;

      if (boundaryRatio < rowRatio) {
        // boundary is taller than row ratio. Widen boundary on each horizontal side
        let ratioAdjustment = ((boundaryHeight * rowRatio) - boundaryWidth) / 2;
        minX = minX - ratioAdjustment;
        maxX = maxX + ratioAdjustment;
      } else {
        // boundary is wider than row ratio. Heighten boundary on each vertical side
        let ratioAdjustment = ((boundaryWidth / rowRatio) - boundaryHeight) / 2;
        minY = minY - ratioAdjustment;
        maxY = maxY + ratioAdjustment;
      }

      // Now that we have the boundaries determine the zoom level...
      boundaryWidth = maxX - minX;

      defaultZoomModifier = (containerWidth / boundaryWidth);
      defaultZoomEffect = defaultZoomModifier * 100 + "%";

      // And convert minX and minY to Top and Left for the new zoom level
      defaultTop = minY * defaultZoomModifier * -1;
      defaultLeft = minX * defaultZoomModifier * -1;

      // hack adjustment for now based off 0.65 for 100% zoom
      defaultFontSize = ((( $(window).innerWidth() * fontRatio * defaultZoomModifier ) / 100 ) + 'px');

      setTimeout( function() {
        $('.map-circle-text').css( 'font-size', defaultFontSize );
        mapContainer.animate( { top: defaultTop, left: defaultLeft, width: defaultZoomEffect }, zoomSpeed, function() { $('.location-svg').addClass('isZoomed'); redrawMap(); /*unblur(); */} );
        //mapRow.animate( { backgroundSize: defaultZoomEffect, 'background-position-x': defaultLeft, 'background-position-y': defaultTop }, zoomSpeed);
        //$('.map-circle-text').css( 'font-size', newFontSize );
        //mapContainer.animate( { top: calculatedTop, left: calculatedLeft, width: defaultZoomLevel }, zoomSpeed, function() { $('.location-svg').addClass('isZoomed'); redrawMap(); /*unblur(); */} );
        //mapRow.animate( { backgroundSize: defaultZoomLevel, 'background-position-x': calculatedLeft, 'background-position-y': calculatedTop }, zoomSpeed);
      }, zoomDelay);
	  
    }

    function zoomToDefault() {
	  
	  //blur(); 
	  
      var mapContainer = $('#map-container');
      var map = $('#map');
      var mapRow = $('#map-row');
      var newTop = map.height() - defaultMapHeight;

      if (!loading) {
        setTimeout( function() {
       $('.map-circle-text').css( 'font-size', defaultFontSize );
       mapContainer.animate( { width: defaultZoomEffect, top: defaultTop, left: defaultLeft }, zoomSpeed, function() {   redrawMap(); /* unblur();*/ });
       //mapRow.animate( { backgroundSize: defaultZoomEffect, 'background-position-x': defaultTop, 'background-position-y': defaultLeft }, zoomSpeed);

        }, zoomDelay);
        
      } else {
        mapContainer.css('top', newTop);

        var mapRow = $('#map-row');
        var mapImage = $('#map-image');

        //mapRow.css('background-image', 'url(' + mapImage.attr('src') + ')');
       
        loading = false;
      }

    }

    // CSS only solutions will break on page resize
    // handle map container height dynamically
    function adjustMapHeight() {
      var mapImage = $('#map-image');
      var map = $('#map');
      var mapRow = $('#map-row');

      if ($(window).width() <= 1024 && $(window).height() <= 768) {
        // Breakpoint: Set row height to 90% window height
        map.height($(window).height() * .8);
        mapRow.height($(window).height() * .8);
      } else {
        map.height(mapImage.height() * (1 - northPoleMargin));
        mapRow.height(map.height());
      }

      defaultMapHeight = mapImage.height();
      defaultMapWidth = mapImage.width();
      calculateDefaultBoundaries();

      hideDrawers();
    }
    
   /* function blur() {
	    $("#map-container > svg").fadeTo(zoomSpeed, 0.5);
	    tweenBlur($("#map-image"),0, 1);
	    console.log("BLURRED!");
    }
    function unblur() {
	    $("#map-container > svg").fadeTo(zoomSpeed, 1);
	    tweenBlur($("#map-image"),1, 0);
	    console.log("UNBLURRED!");
    }
    
    
    var setBlur = function(ele, radius) {
        $(ele).css({
           "-webkit-filter": "blur("+radius+"px)",
            "filter": "blur("+radius+"px)"
       });
   },

   // Generic function to tween blur radius
   tweenBlur = function(ele, startRadius, endRadius) {
        $({blurRadius: startRadius}).animate({blurRadius: endRadius}, {
            duration: zoomSpeed,
            easing: 'swing', // or "linear"
                             // use jQuery UI or Easing plugin for more options
            step: function() {
                setBlur(ele, this.blurRadius);
            },
            callback: function() {
                // Final callback to set the target blur radius
                 // jQuery might not reach the end value
                 setBlur(ele, endRadius);
            }
        });
    };*/
    
    function redrawMap() {
	    setTimeout(function() {
		    $("#map-container > svg").hide().show(0);
	    },500);
    }
	
	function hideTitle() {
		$('.story-headline').removeClass('isVisible');
	}
	
	function readMore() {
		
		setTimeout( function() {
			var storyInfoBody = $(".story-info").prop('scrollHeight');
			var mapHeight = ( $("#map-row").height() + 5 );
			console.log(storyInfoBody + " " + mapHeight);
			if(storyInfoBody >= mapHeight) {
				$(".read-more").addClass("isVisible");
			}
		}, 500);
		
	}
	
	var	 storyScrollTimeout = null;
	var  didStoryScroll = false;
	function hasScrolledStory() {
		
		if (storyScrollTimeout) clearTimeout(storyScrollTimeout);
		storyScrollTimeout = setTimeout(function(){
	  		if( $('.story-info').scrollTop() + $('.story-info').innerHeight() >= $('.story-info')[0].scrollHeight )
            {
                $('.read-more').removeClass("isVisible");
            }
		}, 1000);
		
		
	}
	$(".story-info").bind("scroll", function(event){
	    didStoryScroll = true;
	});
	
	setInterval(function() {
	    if (didStoryScroll) {
	        hasScrolledStory();
	        didStoryScroll = false;
	    }
	}, 250);
	
   $(window).bind('resizeend', function() {
      setTimeout(function() {
      adjustMapHeight();
      readMore();
    }, 1000);
   });
  if(loading) {
    setTimeout(function() {
    adjustMapHeight();
  }, 1000);
}
    
   

    // Using drawer animations as triggers so we can animate a closing drawer before
    // opening up a new one
    $("#story-info, #location-info, #filter-drawer").on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', 
      function() {
        // If the drawer is hiding due to a different location being chosen, now show that location info
        if (selectedStoryId != 0 && !$('#story-info').hasClass('isVisible')) {
          showSelectedStory();
        } else if (selectedLocationId != 0 && selectedStoryId == 0 && !$('#location-info').hasClass('isVisible')) {
          showSelectedLocation();
        }
    });
  });
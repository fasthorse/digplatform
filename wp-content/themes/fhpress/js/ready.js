// add general-use js/jquery here
// anything within the ready function can safely access jQuery via $
jQuery(document).ready(function($) {
	
	var winwidth = $(window).width();
	var winheight = $(window).height();
	var breakpoint = 800;
	
	$('.header').addClass('isVisible').css({'height': 'auto'});
		
	$(".modal-close, .overlay").bind("click", function(e) {
		e.preventDefault();
		$(".modal").removeClass('isVisible');
		player.stop();
		
	});
	$(".play").bind("click", function(e) {
		e.preventDefault();
		$(".modal").addClass('isVisible');
		console.log($(this).data('video-src'));
		newSource = "//www.youtube.com/embed/" + $(this).data('video-src') + "?playsinline=0&enablejsapi=1";
		player.source = {
		    type: 'video',
		    sources: [
		        {
		            src: $(this).data('video-src'),
		            provider: 'youtube',
		            autoplay:true
		        },
		    ],
		};
		setTimeout(function (){
			player.play();
		}, 1200);
	});
	
	// equal height columns
	$.fn.setAllToMaxHeight = function(){
		return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height(); }) ) );
	};
	
	var player = new Plyr('#player', {
	    
		
	});
	
	var state = null;
	var newstate = null;
	var paused = false;
	
	player.on('statechange', function(event) {
		state = event.detail.code;
		
		
		if( state === 2 ) {
			paused = true;
		
		} else {
			paused = false;
		}
		
		
		
		
		
				
		if( state === 0 ) {
			
			//console.log(event.detail.code);
			//console.log("paused!");
			player.stop();
			$('.modal').removeClass('isVisible');

			
		}
	});
	player.on('ready', function(event) {
		setInterval(function () {
			
			if( state !== 2) {
				paused = false;
			}
			
			if( paused === true && state === 2 ) {
				player.stop();
				$('.modal').removeClass('isVisible');
			
			}
		}, 2000);
		
	});
	
	var storyPlayer = new Plyr('#story-info-video-container', {
	
		
	});
	
	
	$('.nav a').click(function(e){
		e.preventDefault();
	    var target = $(this).attr('href');
	    var targetLoc = $(target).offset().top;
	    $('html, body').stop().animate({ scrollTop: targetLoc }, {duration: 1000, easing: "swing"});
	    
	});

	
	
	var scrollTimeout = null;
	var didScroll = false;
	var didSticky = false;
	
	function hasScrolled() {
				
	    $('.nav').removeClass("hideNav");
	    
	    if (scrollTimeout) clearTimeout(scrollTimeout);
		scrollTimeout = setTimeout(function(){
	  		if(!$('.nav').hasClass("hideNav") && $('.sticky-wrapper').hasClass('scrollFixed')) {  
	  			$('.nav').addClass("hideNav");
	  		}
		}, 3000);
		
		toggleStoryHeadline();
		
	}
	
	function stickyNav() {
		
		if(!didSticky) {
			$('.nav').sticky({
				widthFromWrapper : false,
				getWidthFrom : 'body',
				className : 'scrollFixed'
			});		
		}
		didSticky = true;
	}
	$(window).bind("scroll", function(event){
	    didScroll = true;
	});
	
	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);
	
	var mapRow = $("#map-row");
	function toggleStoryHeadline() {
		
 		if ( mapRow.is( ':in-viewport(-200)' ) ) {
			mapRow.find('.story-headline').removeClass('isVisible');
		} else {
			mapRow.find('.story-headline').addClass('isVisible');
		}
	}
		
	// load and resizeEnd - put anything here that needs the resizeEnd function
	$(window).bind('load', function() {
			
		setTimeout(function() {
			$('.post').each(function(){
				$(this).css({'height':'auto'});
			});
		$('.post').setAllToMaxHeight();	
		}, 500);		

		switch( true ){
			case( winwidth > breakpoint ):
				
				stickyNav();
						
			break;
			default:
				
				
			break;
		}
		
	});
	
	$(window).bind('resizeend', function() {
		console.log("resized");
		
		setTimeout(function() {
			
			$('.post').each(function(){
				$(this).css({'height':'auto'});
			});
		$('.post').setAllToMaxHeight();	
		}, 500);
		
		winwidth = $(window).width();
		winheight = $(window).height();
		
		switch( true ){
			case( winwidth > breakpoint ):
			
				stickyNav();	
					
			break;
			default:
				//do mobile stuffs by default
		
				if(didSticky) {
		
					$('.nav').unstick();
					didSticky = false;
				}
				
			break;
		}
	});

	// responsive videos - maintains aspect ratio on load and resize for fluid videos
	var	$allVideos = $("iframe"),
		$fluidEl = $("body");

	// Figure out and save aspect ratio for each video
	$allVideos.each(function() {
		$(this)
			.data('aspectRatio', this.height / this.width)
			.removeAttr('height')
			.removeAttr('width');
	});

	// When the window is resized
	$(window).resize(function() {
		// Resize all videos according to their own aspect ratio
		$allVideos.each(function() {
			var $el = $(this);
			var newWidth = $el.parent().width();
			$el.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));
		});

	// Kick off one resize to fix all videos on page load
	}).resize();



	/* wow! much konami! very code! */
	var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";
	$(document).keydown(function(e) {
		kkeys.push( e.keyCode );
		if ( kkeys.toString().indexOf( konami ) >= 0 ){
			$(document).unbind('keydown',arguments.callee);
			console.log( '30 lives!!' );
		}
	});
	
	
	
});
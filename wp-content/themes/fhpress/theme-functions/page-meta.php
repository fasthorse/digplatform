<?php


$screens = array( 'post', 'page', 'custom_post_type' );


/* OUTPUT */

function construct_meta() {
	
	/* URL */
	$page_meta_url = get_bloginfo('url').$_SERVER["REQUEST_URI"] ;
	
	/* TWITTER */
	$option_twitter_handle = get_option('twitter_handle');
	
	/* TITLE */
	$fhpress_meta_title = get_post_meta( get_the_ID() , 'page_title', true );
    if( $fhpress_meta_title == '' ):
    	$fhpress_meta_title = get_the_title() . " | " . get_bloginfo('name');
    endif;
    if( !is_single() ) {
	    $fhpress_meta_title .=  " | " . get_bloginfo('description');
    }
    
    $fhpress_meta_title = esc_attr( strip_tags( stripslashes( $fhpress_meta_title ) ) );
    
    /* KEYWORDS */
    $fhpress_meta_keywords = get_post_meta( get_the_ID() , 'page_tags', true );
    if( $fhpress_meta_keywords == '' ):
    	$fhpress_meta_keywords = '';
    endif;
    
    $fhpress_meta_keywords = esc_attr( strip_tags( stripslashes( $fhpress_meta_keywords ) ) );
    
    /* DESC */
    $fhpress_meta_description = get_post_meta( get_the_ID() , 'page_description', true );
    if( $fhpress_meta_description == '' ):
		$fhpress_meta_description = get_bloginfo( 'description' );
    endif;
    
    $fhpress_meta_description =  esc_attr( strip_tags( stripslashes( $fhpress_meta_description ) ) );
    
    /* THUMB */
    $fhpress_share_thumb = get_post_meta( get_the_ID() , 'share_thumb', true );
    if( $fhpress_share_thumb == '' ):
    	$fhpress_share_thumb = '';
    endif;
    
    $fhpress_share_thumb = esc_attr( strip_tags( stripslashes( $fhpress_share_thumb) ) );
	
	
	/* Echo */
	echo '<meta charset="' . get_bloginfo( 'charset' ) .'" />'
	. '<link rel="icon" href="' . get_bloginfo('stylesheet_directory') . '/favicon.png" type="image/x-png" />'
	. '<meta name="viewport" content="initial-scale=1, maximum-scale=1">';	

}
add_action('wp_head', 'construct_meta', 1);





	<footer class="footer row">
			<div class="col span8">
				<small>Copyright &copy; <?php echo date("Y") ?> <a href="https://cargill.com" title="<?php bloginfo('description'); ?>">Cargill, Inc.</a> <?php _e('All Rights Reserved.'); ?></small>
			</div>
			<div class="col span4">
				<ul class="h-list cf nav">
					
				</ul>
			</div>
	</footer>
	
	
	
	<?php wp_footer();?>

</body>
</html>
<?php

// filter categories to be used globally
const FILTER_CATEGORIES = array(
    'technology' => 'Technology',
    'impact'     => 'Impact'
);

include('theme-functions/page-meta.php');

add_filter('show_admin_bar', '__return_false');

add_filter( 'frm_load_dropzone', '__return_false' );

//add_filter( 'user_can_richedit' , '__return_false', 50 );

// get rid of huge header image
function fhpress_remove_custom_header() {
	remove_theme_support( 'custom-header' );
}
add_action( 'after_setup_theme', 'fhpress_remove_custom_header', 12 );

function the_title_trim($title) {

	$title = esc_attr($title);

	$findthese = array(
		'#Protected:#',
		'#Private:#'
	);

	$replacewith = array(
		'', // What to replace "Protected:" with
		'' // What to replace "Private:" with
	);

	$title = preg_replace($findthese, $replacewith, $title);
	return $title;
}
add_filter('the_title', 'the_title_trim');

// disable that pesky wysiwyg editor!
//add_filter( 'user_can_richedit' , create_function ( '$a', 'return false;' ), 50 );

//custom excerpt length
function custom_excerpt_length( $length ) {
return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//add prev_next link styling
add_filter('next_post_link', 'post_link_attributes');
add_filter('previous_post_link', 'post_link_attributes');
add_filter('next_posts_link', 'post_link_attributes');
add_filter('previous_posts_link', 'post_link_attributes');
 
function post_link_attributes($output) {
    $code = 'class="btn btn-primary"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

// remove p tags from images
function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// disable xmlrpc
add_filter( 'xmlrpc_methods', function( $methods ) {
   unset( $methods['pingback.ping'] );
   return $methods;
} );

// Remove new emoji junk
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Remove new embed junk
function disable_embeds_init() {

    // Remove the REST API endpoint.
    remove_action('rest_api_init', 'wp_oembed_register_route');

    // Turn off oEmbed auto discovery.
    // Don't filter oEmbed results.
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

    // Remove oEmbed discovery links.
    remove_action('wp_head', 'wp_oembed_add_discovery_links');

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action('wp_head', 'wp_oembed_add_host_js');
}
add_action('init', 'disable_embeds_init', 9999);

// add scripts and styles we DO want
function fhpress_style_script() {
	wp_dequeue_style( 'genericons' );
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//code.jquery.com/jquery-3.3.1.min.js', false, '3.3.1');
	wp_enqueue_script('jquery');
	wp_register_script('jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', false, '1.12.1');
	wp_enqueue_script('jquery-ui');
	
	wp_enqueue_style( 
		'fhpress-style', 
		get_stylesheet_directory_uri() . '/style.min.css',
		array() 
	);

	wp_enqueue_script(
		'fhpressplugins',
		get_stylesheet_directory_uri() . '/js/final.min.js',
		array( 'jquery' )
	);
	
	wp_enqueue_script(
		'plyr',
		get_stylesheet_directory_uri() . '/js/plyr.polyfilled.min.js',
		array()
	);

	wp_enqueue_script(
		'stories-dev-script',
		get_stylesheet_directory_uri() . '/js/stories.js',
		array()
	);

  // We won't be needing this if we aren't getting their videos from youtube
  wp_register_script('youtube', '//www.youtube.com/iframe_api');
  wp_enqueue_script('youtube');
}
add_action( 'wp_enqueue_scripts', 'fhpress_style_script', 11 );

//add excerpt to pages
add_post_type_support('page', 'excerpt');

//add support for custom thumbnails
add_theme_support('post-thumbnails');

//custom size
add_image_size( 'story-thumbnail', 900, 720, array( 'center', 'top' ) );

// add menus
add_theme_support( 'menus' );
if( function_exists( 'register_nav_menus' ) ):
	register_nav_menus(
		array(
			'primary-nav' => 'Primary',
			'secondary-nav' => 'Secondary',
			'contact-nav' => 'Contact',
			'footer-nav' => 'Footer'
		)
	);
endif;

// add widgets
function fhpress_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Widgets', 'fhpress' ),
		'id'            => 'sidebar',
		'description'   => __( 'Appears in the sidebar.', 'fhpress' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	) );

	register_sidebar( array(
		'name'          => __( 'The small print', 'fhpress' ),
		'id'            => 'the-small-print',
		'description'   => __( 'Copyright info, etc..', 'fhpress' ),
		'before_widget' => '<small id="%1$s" class="widget %2$s">',
		'after_widget'  => '</small>',
		'before_title'  => '<h3 class="widget-title">'
	) );
}
add_action( 'widgets_init', 'fhpress_widgets_init' );

/* custom wp login for presencemaker */
function custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('stylesheet_directory').'/css/custom-login.css" />'; 
}
add_action('login_head', 'custom_login');

function my_login_logo_url() {
    return 'http://www.fasthorseinc.com';
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_links() {
    echo '<p id="assistance"><a href="mailto:joer@fasthorseinc.com">Email for assistance</a></p>'; 
}
add_filter( 'login_form', 'my_login_links' );

//add_action( 'init', 'people_init' );

// Register Custom Post Type
function case_studies() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Case Studies', 'text_domain' ),
		'name_admin_bar'        => __( 'Case Studies', 'text_domain' ),
		'archives'              => __( 'Case Study Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Case Study:', 'text_domain' ),
		'all_items'             => __( 'All Case Studies', 'text_domain' ),
		'add_new_item'          => __( 'Add New Case Study', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Case Study', 'text_domain' ),
		'edit_item'             => __( 'Edit Case Study', 'text_domain' ),
		'update_item'           => __( 'Update Case Study', 'text_domain' ),
		'view_item'             => __( 'View Case Study', 'text_domain' ),
		'search_items'          => __( 'Search Case Study', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Thumbnail', 'text_domain' ),
		'set_featured_image'    => __( 'Set thumbnail', 'text_domain' ),
		'remove_featured_image' => __( 'Remove thumbnail', 'text_domain' ),
		'use_featured_image'    => __( 'Use as thumbnail', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'text_domain' ),
		'items_list'            => __( 'Case Study List', 'text_domain' ),
		'items_list_navigation' => __( 'Case Study List Navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter products list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Case Studies', 'text_domain' ),
		'description'           => __( 'Case Studies', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'rewrite' => array('slug' => 'work', 'with_front' => false),
	);
	register_post_type( 'case_studies', $args );

}
add_action( 'init', 'case_studies', 0 );

// Location post type.
// Each Location represents a point on the map and may contain one or more location stories
function create_location_posttype() {
    register_post_type( 'location',
        array(
            'labels' => array(
                'name' => __( 'Locations' ),
                'singular_name' => __( 'Location' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'location'),
            'menu_position' => 6
        )
    );
}

// Remove regular post type and comments.  We aren't using them.
add_action( 'admin_menu', 'remove_admin_menus' );

//Remove top level admin menus
function remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'edit.php' );
}

// Location Story post type.
// Each Location Story represents one interactive story, which are collected in locations
function create_location_story_posttype() {
    register_post_type( 'location_story',
        array(
            'labels' => array(
                'name' => __( 'Location Stories' ),
                'singular_name' => __( 'Location Story' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'story'),
            'menu_position' => 7
        )
    );
}

// Register custom taxonomies for stories
// One for each higher level category (technology, enterprise, impact)
// This is more rigid engineering wise but should make for simpler content entry
function register_filter_taxonomies() {
  foreach (FILTER_CATEGORIES as $index => $value) {
    $labels = [
      'name'              => _x($value . ' Tags', 'taxonomy general name'),
      'singular_name'     => _x($value . ' Tag', 'taxonomy singular name'),
      'search_items'      => __('Search Tags'),
      'all_items'         => __('All Tags'),
      'parent_item'       => __('Parent Tag'),
      'parent_item_colon' => __('Parent Tag:'),
      'edit_item'         => __('Edit Tag'),
      'update_item'       => __('Update Tag'),
      'add_new_item'      => __('Add New Tag'),
      'new_item_name'     => __('New Tag Name'),
      'menu_name'         => __($value . ' Tags'),
    ];
    $args = [
      'hierarchical'      => false,
      'labels'            => $labels,
      'show_ui'           => true,
      'show_admin_column' => false,
      'query_var'         => true,
      'rewrite'           => ['slug' => $index . '_tag'],
    ];
    register_taxonomy($index . '_tags', ['location_story'], $args);
  }
}

add_action('init', 'register_filter_taxonomies');

// Add a default field for the map_image field
// This is so we can default the map_image to always be the same (the map used in the
// map module) and then hide the map_image field so the user can't edit it.
add_action('acf/render_field_settings/type=image', 'add_default_value_to_map_image');
function add_default_value_to_map_image($field) {
  $instructions = 'Default image for this field.';
  if ($field['name'] == 'map_image') {
    $instructions = 'This should be set to the map image being used in the map module';
  }

  acf_render_field_setting( $field, array(
    'label'      => 'Default Image',
    'instructions'    => $instructions,
    'type'      => 'image',
    'name'      => 'default_value',
  ));
}

add_action( 'init', 'create_location_story_posttype' );
add_action( 'init', 'create_location_posttype' );

add_filter( 'meta_content', 'wptexturize');
add_filter( 'meta_content', 'convert_smilies');
add_filter( 'meta_content', 'convert_chars');
add_filter( 'meta_content', 'wpautop');
add_filter( 'meta_content', 'shortcode_unautop');
add_filter( 'meta_content', 'prepend_attachment');
add_filter( 'meta_content', 'do_shortcode');

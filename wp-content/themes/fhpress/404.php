<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
*/
$search_term = substr($_SERVER['REQUEST_URI'],1);
$search_term = urldecode(stripslashes($search_term));
$find = array ("'.html'", "'.+/'", "'[-/_]'") ;
$replace = " " ;
$search_term = trim(preg_replace ( $find , $replace , $search_term ));
get_header();?>

<main role="main" class="main page">
	<section role="section" class="onScreen content white about">
		<div class="row fullwidth center">
			<div class="col span12 ">
				<div class="valign">
					<div class="vcenter">
						<h1>We're sorry! We seem to be having trouble finding that!</h1>
						<p>It's possible something has moved or there was a problem copying/pasting the address. You can head to the <a href="/" title="">homepage</a>, try a site search, or we've provided some suggestions based on the URL you were trying to view.</p>
						<br/><br/><br/><br/>
						<h5>Try a search</h5>
						<?php get_search_form(); ?>
			<br/><br/><br/><br/>
						<h4>See Our Suggestions</h4>
						<ul>
							<?php $search404 = new WP_Query( 's=' . $search_term );
							if( $search404->have_posts() ):  while( $search404->have_posts() ): $search404->the_post();?>
							<li>
								<p><strong><a href="<?=the_permalink();?>" title=""><?=the_title();?></a></strong></p>
								<p><?=get_the_excerpt();?></p>
								<br/><br/>
							</li>
							<?php endwhile; endif; wp_reset_query();?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
		
<?php get_footer();?>
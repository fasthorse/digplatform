<?php
/* Template Name: Home
 *
*/
get_header(); ?>


<main role="main" class="main page home">	
	
	<?php get_template_part( 'partials/stories' ); ?>
	
	<?php get_template_part( 'partials/mission' ); ?>
	
	<?php get_template_part( 'partials/news' ); ?>
    
	<?php wp_reset_query(); ?>
</main>
<?php get_footer(); ?>
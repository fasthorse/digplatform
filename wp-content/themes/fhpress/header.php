<?php header('X-UA-Compatible: IE=edge'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	
	<!-- Pulls from page-meta.php now!  -->
	
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
		
		<header class="header valign isVisible" style="background-image:url(<?php echo get_field('hero_background'); ?>);">
			<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="vcenter">
				
				<h1 class="logo"><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/cargill_logo.svg" alt="Cargill"/></h1>
				
				<div class="content">
					<h2>Innovation For A Thriving World</h2>
					<a href="#" id="play" class="play btn btn-fill round" data-video-src="<?php echo get_field('anthem_video_id'); ?>"><span class="icon icon-play"></span> Play Anthem</a>
				</div>
				
				<ul class="h-list cf nav">
					<li><a href="#map-row"><span class="icon icon-stories"></span> Our Stories</a></li>
					<li><a href="#mission"><span class="icon icon-mission"></span> Our Mission</a></li>
					<li><a href="#news"><span class="icon icon-news"></span> News & Updates</a></li>
				</ul>
				
				
			</div>
			<?php endwhile; endif; wp_reset_query(); ?>

			
		</header>
				
		<?php get_template_part( 'partials/video' ); ?>
		
		